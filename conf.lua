function love.conf(t)
  t.version = '11.1'
  if t.window then
    t.window.title = 'In Orbit'
  end
end

if not lutro then
  lutro = love
  require'compat'
end
