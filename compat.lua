function love.keypressed(key)
  if key == 's' then lutro.joystickpressed(0, 0)
  elseif key == 'a' then lutro.joystickpressed(0, 1)
  elseif key == 'tab' then lutro.joystickpressed(0, 2)
  elseif key == 'return' then lutro.joystickpressed(0, 3)
  elseif key == 'd' then lutro.joystickpressed(0, 8)
  elseif key == 'w' then lutro.joystickpressed(0, 9)
  elseif key == 'q' then lutro.joystickpressed(0, 10)
  elseif key == 'e' then lutro.joystickpressed(0, 11)
  end
end

function love.keyreleased(key)
  if key == 's' then lutro.joystickreleased(0, 0)
  elseif key == 'a' then lutro.joystickreleased(0, 1)
  elseif key == 'tab' then lutro.joystickreleased(0, 2)
  elseif key == 'return' then lutro.joystickreleased(0, 3)
  elseif key == 'd' then lutro.joystickreleased(0, 8)
  elseif key == 'w' then lutro.joystickreleased(0, 9)
  elseif key == 'q' then lutro.joystickreleased(0, 10)
  elseif key == 'e' then lutro.joystickreleased(0, 11)
  end
end
